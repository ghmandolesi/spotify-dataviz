const initialX = 50;
const total = 15;
const BKG_COLOR = "#1F2124";

let artists;
let button;
let avgPlaysPerDay;
let avgPlayMinutesPerDay;
let logo;
let totalArtistPlays;
let totalPlays;
let totalPlayMinutes;

// Little hack to wait for DB loading
let delayToLoadDB = new Promise(function (resolve, reject) {
  setTimeout(resolve, 1000);
});

function preload() {
  logo = loadImage("assets/spotify-logo.png");
}

async function setup() {
  await delayToLoadDB;
  createCanvas(900, 800);

  // Load data
  totalPlays = await getTotalPlays();
  totalPlayMinutes = millisToMinutes(await getTotalPlayMilliseconds());
  const totalDays = await getTotalDays();
  avgPlaysPerDay = Math.ceil(totalPlays / totalDays);
  avgPlayMinutesPerDay = Math.ceil(totalPlayMinutes / totalDays);
  ({ totalArtistPlays, artists } = await getTopArtists(total));

  button = createButton("Save Image");
  button.position(width / 2, height + 10);
  button.mousePressed(saveImage);
}

function draw() {
  background(BKG_COLOR);

  // Draw Spotify logo
  imageMode(CORNER);
  image(logo, initialX, 50, 100, 100);

  // Draw title
  fill(255);
  textSize(36);
  textAlign(LEFT);
  text("YOUR 1-YEAR SPOTIFY DASHBOARD", 180, 120);

  // Draw cards
  drawCard(initialX, 180, "Total Plays", totalPlays);
  drawCard(initialX + 133, 180, "Total Play Minutes", totalPlayMinutes);
  drawCard(initialX + 332, 180, "Avg Plays Per Day", avgPlaysPerDay);
  drawCard(
    initialX + 535,
    180,
    "Avg Play Minutes Per Day",
    avgPlayMinutesPerDay
  );

  // Favorite artists title
  fill(0);
  rect(50, 280, 795, 40);
  fill(255);
  textSize(24);
  textAlign(CENTER);
  text("FAVORITE ARTISTS", 445, 308);

  if (artists) {
    textSize(12);
    textAlign(RIGHT);
    const maxTextWidth = findMaxArtistTextWidth();
    let currentY = 355;

    artists.forEach((artist) => {
      text(artist.artistName, initialX + maxTextWidth, currentY);
      currentY += 24;
    });

    currentY = 341;
    const chartInitialX = initialX + maxTextWidth + 20;
    const chartFinalX = 795;
    const chartWidth = chartFinalX - chartInitialX;
    const chartRange = Math.ceil(artists[0].count / 100) * 100;
    artists.forEach((artist) => {
      const value = map(artist.count, 0, chartRange, 0, chartWidth);
      fill("#2AAC56");
      rect(chartInitialX, currentY, value, 20);
      fill(255);
      text(artist.count, chartInitialX + value - 5, currentY + 15);
      currentY += 24;
    });

    noLoop();
  }
}

function mouseMoved() {
  return false;
}

function drawCard(x, y, title, value) {
  textSize(20);
  textStyle(NORMAL);
  textAlign(LEFT);
  let titleWidth = textWidth(title);

  fill("#C2C2C2");
  rect(x, y, titleWidth + 26, 87);

  fill(BKG_COLOR);
  text(title, x + 10, y + 30);

  textSize(36);
  textStyle(BOLD);
  text(value, x + 10, y + 70);
}

function findMaxArtistTextWidth() {
  let maxTextWidth = 0;
  artists.forEach((artist) => {
    let titleWidth = textWidth(artist.artistName);
    if (titleWidth > maxTextWidth) {
      maxTextWidth = titleWidth;
    }
  });
  return maxTextWidth;
}

function saveImage() {
  saveCanvas("spotify-dashboard", "jpg");
}
