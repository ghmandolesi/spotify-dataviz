const getDateRange = async () => {
  const { init, end } = await getTimeRange();
  const initDate = new Date(init);
  const endDate = new Date(end);

  const initYear = initDate.getFullYear();
  const initMonth = initDate.getMonth();
  let endYear = endDate.getFullYear();
  let endMonth = endDate.getMonth();
  if (endMonth === 12) {
    endMonth = 1;
    endYear++;
  } else {
    endMonth++;
  }

  const rangeInitDate = new Date(`${initYear}-${initMonth}-01`);
  const rangeEndDate = new Date(`${endYear}-${endMonth}-01`);
  return { rangeInitDate, rangeEndDate };
};

const millisToMinutes = (millis) => {
  return Math.floor(millis / 60000);
}

const monthDiff = (d1, d2) => {
  let months;
  months = (d2.getFullYear() - d1.getFullYear()) * 12;
  months -= d1.getMonth();
  months += d2.getMonth();
  return months <= 0 ? 0 : months;
};
