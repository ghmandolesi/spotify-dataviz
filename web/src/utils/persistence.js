let db;

// Initialize database
(async function () {
  const sqlPromise = initSqlJs({
    locateFile: (file) => `./src/libraries/${file}`,
  });
  const dataPromise = fetch("spotify.sqlite").then((res) => res.arrayBuffer());
  const [SQL, buf] = await Promise.all([sqlPromise, dataPromise]);
  db = new SQL.Database(new Uint8Array(buf));
})();

const getArtistData = async (artistName) => {
  const query = `SELECT trackName, msPlayed, endTime, timeStamp FROM tracks WHERE artistName = "${artistName}"`;
  let tracks = [];
  const stmt = db.prepare(query);
  while (stmt.step()) {
    const row = stmt.getAsObject();
    tracks.push(row);
  }
  return tracks;
}

const getTimeRange = async () => {
  const query = `SELECT MIN(timeStamp) as init, MAX(timeStamp) as end FROM tracks`;
  const stmt = db.prepare(query);
  stmt.step();
  return stmt.getAsObject();
};

const getTopArtists = async (total = 10) => {
  const query = `SELECT DISTINCT count(*) as count, artistName FROM tracks GROUP BY artistName ORDER BY count DESC LIMIT ${total}`;
  const stmt = db.prepare(query);
  let artists = [];
  let totalArtistPlays = 0;
  while (stmt.step()) {
    const row = stmt.getAsObject();
    artists.push(row);
    totalArtistPlays += row.count;
  }
  return { totalArtistPlays, artists };
}

const getTopArtistsByYearMonth = async () => {
  const query = `SELECT COUNT(*) as count, artistName, STRFTIME('%Y-%m', date(t.endTime)) as monthdate FROM tracks t GROUP BY monthdate ORDER BY monthdate DESC`;
  let artistName = [];
  const stmt = db.prepare(query);
  while (stmt.step()) {
    const row = stmt.getAsObject();
    artistName.push(row);
  }
  return artistName;
};

const getTopTracks = async (total = 100) => {
  const query = `SELECT COUNT(*) as count, trackName, artistName FROM tracks GROUP BY trackName ORDER BY count DESC LIMIT ${total}`;
  let tracks = [];
  const stmt = db.prepare(query);
  while (stmt.step()) {
    const row = stmt.getAsObject();
    tracks.push(row);
  }
  return tracks;
};

const getTopTracksByArtist = async (artistName, total = 10) => {
  const query = `SELECT COUNT(*) as count, trackName FROM tracks where artistName = "${artistName}" GROUP BY trackName ORDER BY count DESC LIMIT ${total}`;
  let tracks = [];
  const stmt = db.prepare(query);
  while (stmt.step()) {
    const row = stmt.getAsObject();
    tracks.push(row);
  }
  return tracks;
}

const getTotalDays = async () => {
  const query = `SELECT COUNT(DISTINCT STRFTIME('%Y-%m-%d', date(t.endTime))) FROM tracks t`;
  const stmt = db.prepare(query);
  stmt.step();
  return stmt.get()[0];
}

const getTotalPlayMilliseconds = async () => {
  const query = `SELECT SUM(msPlayed) FROM tracks`;
  const stmt = db.prepare(query);
  stmt.step();
  return stmt.get()[0];
}

const getTotalPlays = async () => {
  const query = `SELECT count(*) FROM tracks`;
  const stmt = db.prepare(query);
  stmt.step();
  return stmt.get()[0];
}