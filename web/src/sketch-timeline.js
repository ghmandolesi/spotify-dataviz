let artistName;
let rangeInitDate, rangeEndDate;
let rangeInitTime, rangeEndTime;
let scaleInitX, scaleEndX;
let numberOfMonths;
let shouldDraw = true;
let dateMap;
let sel;

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
const border = 150;

// Little hack to wait for DB loading
let delayToLoadDB = new Promise(function (resolve, reject) {
  setTimeout(resolve, 1000);
});

async function setup() {
  await delayToLoadDB;
  createCanvas(1440, 800);

  const topArtists = await getTopArtists();

  sel = createSelect();
  sel.position(10, 10);
  topArtists.artists.forEach((artist) => {
    sel.option(artist.artistName);
  });
  sel.changed(artistSelected);

  await updateData();
}

async function updateData() {
  artistName = sel.value();
  dateMap = new Map();

  ({ rangeInitDate, rangeEndDate } = await getDateRange());

  numberOfMonths = monthDiff(rangeInitDate, rangeEndDate);
  rangeInitTime = rangeInitDate.getTime();
  rangeEndTime = rangeEndDate.getTime();

  scaleInitX = border;
  scaleEndX = width - border;

  const artistData = await getArtistData(artistName);

  artistData.forEach((element) => {
    const { endTime } = element;
    const date = endTime.substring(0, 10);
    if (!dateMap.has(date)) {
      dateMap.set(date, 1);
    } else {
      const total = dateMap.get(date);
      dateMap.set(date, total + 1);
    }
  });
}

function draw() {
  if (rangeInitDate && shouldDraw) {
    background(0);
    fill(255);
    stroke(255);
    const middleY = height / 2;
    line(scaleInitX, middleY, scaleEndX, middleY);

    fill("#1BD760");
    textAlign(CENTER);
    textSize(36);
    text(artistName, width / 2, 100);

    let theYear = rangeInitDate.getFullYear();
    let theMonth = rangeInitDate.getMonth();
    for (let i = 0; i <= numberOfMonths; i++) {
      const month = theMonth + 1;
      const theDate = new Date(`${theYear}-${month}-01`);
      const theTime = theDate.getTime();
      const posX = map(
        theTime,
        rangeInitTime,
        rangeEndTime,
        scaleInitX,
        scaleEndX
      );
      line(posX, middleY - 5, posX, middleY + 5);
      textSize(13);
      const monthName = months[theMonth];
      text(monthName, posX, middleY + 16);
      if (++theMonth === 12) {
        theYear++;
        theMonth = 0;
      }
    }

    for (let singleDate of dateMap.keys()) {
      const theTime = new Date(singleDate).getTime();
      const posX = map(
        theTime,
        rangeInitTime,
        rangeEndTime,
        scaleInitX,
        scaleEndX
      );
      const total = dateMap.get(singleDate);
      stroke("#1BD760");
      line(posX, middleY - 1, posX, middleY - total * 2);
    }
    shouldDraw = false;
  }
}

async function artistSelected() {
  artistName = sel.value();
  await updateData();
  shouldDraw = true;
}
