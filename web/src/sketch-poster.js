const total = 25;
const BKG_COLOR = "#1F2124";

const colourNames = [
  "#20e95d",
  "#56e784",
  "#098e23",
  "#45535c",
  "#138537",
  "#1f3f2a",
];
const sizes = [20, 40, 60, 80, 90, 100, 120, 140, 160, 180];

let artists;
let button;
let myFont;
let totalArtistPlays;

// Little hack to wait for DB loading
let delayToLoadDB = new Promise(function (resolve, reject) {
  setTimeout(resolve, 2000);
});

function preload() {
  myFont = loadFont("assets/Barrio-Regular.ttf");
}

async function setup() {
  await delayToLoadDB;
  ({ totalArtistPlays, artists } = await getTopArtists(total));
  createCanvas(windowWidth, windowHeight);
  textFont(myFont);

  button = createButton("Save Image");
  button.position(1, 1);
  button.mousePressed(saveImage);
}

function draw() {
  background(BKG_COLOR);

  if (artists) {
    artists.forEach((artist) => {
      drawArtistName(artist);
    });
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === 32) {
    redraw();
  }
}

const drawArtistName = (artist) => {
  fill(random(colourNames));
  stroke(0);
  let size = Math.floor((artist.count / totalArtistPlays) * 100);
  if (size > 9) size = 9;
  textSize(sizes[size]);
  const tw = textWidth(artist.artistName);
  const x = random(20, width - tw);
  const y = random(100, height * 0.9);
  text(artist.artistName, x, y);
};

function saveImage() {
  saveCanvas("spotify-poster", "jpg");
}
