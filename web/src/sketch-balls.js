const total = 160;
let spring = 0.05;
let gravity = 0.03;
let friction = -0.9;
let balls = [];
let isAnimating = true;

const BKG_COLOR = "#1F2124";
const COLORS = [
  "#20e95d",
  "#56e784",
  "#098e23",
  "#45535c",
  "#138537",
  "#1f3f2a",
];

// Little hack to wait for DB loading
let delayToLoadDB = new Promise(function (resolve, reject) {
  setTimeout(resolve, 1000);
});

async function setup() {
  await delayToLoadDB;
  createCanvas(windowWidth, windowHeight);
  const { totalArtistPlays, artists } = await getTopArtists(total);

  let index = 0;

  for (let i = 0; i < total; i++) {
    let diameter = map(artists[i].count, 0, totalArtistPlays, 0, width);
    balls[i] = new Ball(
      artists[i],
      random(width),
      random(height),
      diameter,
      i,
      balls,
      total,
      COLORS[index++]
    );
    if (index === COLORS.length) {
      index = 0;
    }
  }
  noStroke();
  fill(255, 204);
}

function draw() {
  if (isAnimating) {
    background(BKG_COLOR);
    balls.forEach((ball) => {
      ball.collide();
      ball.move();
      ball.display();
    });
  }
}

function mouseMoved() {
  balls.forEach((ball) => {
    ball.contains(mouseX, mouseY);
  });
  return false;
}

function mousePressed() {
  isAnimating = !isAnimating;
};