const BKG_COLOR = "#1F2124";
const COLORS = [
  "#20e95d",
  "#56e784",
  "#098e23",
  "#45535c",
  "#138537",
  "#1f3f2a",
];
const TOTAL = 5;

let tracks;

// Little hack to wait for DB loading
let delayToLoadDB = new Promise(function (resolve, reject) {
  setTimeout(resolve, 1000);
});

async function setup() {
  await delayToLoadDB;
  createCanvas(windowWidth, windowHeight);
  tracks = createTrackObjects(await getTopTracks(TOTAL));
  console.log(tracks);
}

function draw() {
  if (tracks) {
    tracks.forEach((track) => {
      track.move();
      track.display();
    });
  }
}
class Track {
  constructor(artistName, trackName, count, x, y, deltaX, deltaY, topCount, color) {
    this.artistName = artistName;
    this.trackName = trackName;
    this.count = count;
    this.x = x;
    this.y = y;
    this.deltaX = deltaX;
    this.deltaY = deltaY;
    this.topCount = topCount;
    this.color = color;
  }

  move() {}

  display() {}
}

function createTrackObjects(topTracks) {
  const topCount = topTracks[0].count;
  let tracks = [];
  let colorIndex = 0;
  topTracks.forEach(({ artistName, trackName, count }) => {
    tracks.push(
      new Track(
        artistName,
        trackName,
        count,
        random(width),
        random(height),
        random(-1, 1),
        random(-1, 1),
        topCount,
        COLORS[colorIndex]
      )
    );
    if (++colorIndex == COLORS.length) {
      colorIndex = 0;
    }
  });
  return tracks;
}