
## Prerequisites

1) Download and install Node.js
   
   https://nodejs.org/en/download/

2) Install browser-sync

    npm install -g browser-sync

3) Unzip your ```my_spotify_data.zip``` into ```data``` folder. A folder named ```MyData``` will be created.


## Setup
### Create database

    cd data
    node import.js

Information from ```StreamingHistory``` JSON files will be read and a file named ```spotify.sqlite``` created on ```web``` folder.

---
## Running

### Run it to serve the contents

    cd web
    browser-sync start --server -f -w
