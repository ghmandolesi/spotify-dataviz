const fs = require("fs");
const { sep } = require("path");
const initSqlJs = require("../web/src/libraries/sql-wasm.js");
const INPUT_FOLDER = "MyData";

initSqlJs().then(function (SQL) {
  const db = new SQL.Database();
  db.run(
    "CREATE TABLE tracks (artistName, trackName, msPlayed, endTime, timeStamp);"
  );

  let files = [];
  try {
    fs.readdirSync(INPUT_FOLDER).forEach((file) => {
      if (file.startsWith("StreamingHistory")) {
        files.push(file);
      }
    });
  } catch (err) {
    console.error('Could not find folder "MyData". Please verify.');
    process.exit(-1);
  }
  if (!files.length) {
    console.error(
      'Found no "StreamingHistory" file to process. Please verify.'
    );
    process.exit(-1);
  }

  let total = 0;
  files.forEach((file) => {
    const filename = `${INPUT_FOLDER}${sep}${file}`;
    console.log(`Importing ${filename}`);
    const jsonData = JSON.parse(fs.readFileSync(filename, "utf8"));
    for (let i in jsonData) {
      const { artistName, trackName, msPlayed, endTime } = jsonData[i];
      const timeStamp = new Date(endTime).getTime();
      db.run("INSERT INTO tracks VALUES (?,?,?,?,?)", [
        artistName,
        trackName,
        msPlayed,
        endTime,
        timeStamp,
      ]);
      total++;
    }
  });

  const data = db.export();
  const buffer = new Buffer.from(data);
  fs.writeFileSync("../web/spotify.sqlite", buffer);

  console.log(`Finished. ${total} tracks imported.`);
});
